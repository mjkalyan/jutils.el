;;; jutils.el --- A collection of Emacs Lisp utilities

;; Copyright (C) 2022 James Kalyan

;; Author: James Kalyan
;; Keywords: lisp
;; Version 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Provides commonly used utilities when they don't already exist in high-quality packages.

;;; Code:

;;; ---------------------
;;; Interactive functions
;;; ---------------------

(defun jutils:switch-to-mru-buffer ()
  "Switch to most recently visited buffer."
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))

(defun jutils:scroll-down-half-window ()
  "Scroll down half a window."
  (interactive)
  (scroll-up-command (/ (window-body-height) 2)))

(defun jutils:scroll-up-half-window ()
  "Scroll up half a window."
  (interactive)
  (scroll-down-command (/ (window-body-height) 2)))

(defun jutils:delete-visited-file (&optional path force-p)
  "Delete PATH, kill its buffers and expunge it from vc/magit cache.
If PATH is not specified, default to the current buffer's file.
If FORCE-P, delete without confirmation.

Stolen from doom/delete-this-file"
  (interactive
   (list (buffer-file-name (buffer-base-buffer))
         current-prefix-arg))
  (let* ((path (or path (buffer-file-name (buffer-base-buffer))))
         (short-path (abbreviate-file-name path)))
    (unless (and path (file-exists-p path))
      (user-error "Buffer is not visiting any file"))
    (unless (file-exists-p path)
      (error "File doesn't exist: %s" path))
    (unless (or force-p (y-or-n-p (format "Really delete %S?" short-path)))
      (user-error "Aborted"))
    (let ((buf (current-buffer)))
      (unwind-protect
          (progn (delete-file path t) t)
        (if (file-exists-p path)
            (error "Failed to delete %S" short-path)
          ;; Ensures that windows displaying this buffer will be switched to
          ;; real buffers (`doom-real-buffer-p')
          (doom/kill-this-buffer-in-all-windows buf t)
          (doom--update-files path)
          (message "Deleted %S" short-path))))))

(defun jutils:rename-visited-file (new-path &optional force-p)
  "Move current buffer's file to NEW-PATH.
If FORCE-P, overwrite the destination file if it exists, without confirmation.

Stolen from doom/move-this-file"
  (interactive
   (list (read-file-name "Move file to: ")
         current-prefix-arg))
  (unless (and buffer-file-name (file-exists-p buffer-file-name))
    (user-error "Buffer is not visiting any file"))
  (let ((old-path (buffer-file-name (buffer-base-buffer)))
        (new-path (expand-file-name new-path)))
    (when (directory-name-p new-path)
      (setq new-path (concat new-path (file-name-nondirectory old-path))))
    (make-directory (file-name-directory new-path) 't)
    (rename-file old-path new-path (or force-p 1))
    (set-visited-file-name new-path t t)
    (doom--update-files old-path new-path)
    (message "File moved to %S" (abbreviate-file-name new-path))))

(defun jutils:copy-visited-file (new-path &optional force-p)
  "Copy current buffer's file to NEW-PATH.
If FORCE-P, overwrite the destination file if it exists, without confirmation.

Stolen from doom/copy-this-file"
  (interactive
   (list (read-file-name "Copy file to: ")
         current-prefix-arg))
  (unless (and buffer-file-name (file-exists-p buffer-file-name))
    (user-error "Buffer is not visiting any file"))
  (let ((old-path (buffer-file-name (buffer-base-buffer)))
        (new-path (expand-file-name new-path)))
    (make-directory (file-name-directory new-path) 't)
    (copy-file old-path new-path (or force-p 1))
    (doom--update-files old-path new-path)
    (message "File copied to %S" (abbreviate-file-name new-path))))

(defun jutils:completing-read-eshell-history ()
  "Insert a command from the user's Eshell history file at point."
  (interactive)
  (require 'vc)
  (if (and completing-read-function eshell-history-file-name)
      (insert (funcall completing-read-function
                       "Select from Eshell history: "
                       (vc--read-lines eshell-history-file-name)))
    (message "Missing completing-read-function or eshell-history-file-name variables.")))

;;; ---------------
;;; Non-interactive
;;; ---------------

(defun jutils:lookup-password (host user)
  "Retrieve the password matching HOST and USER from the `auth-sources' file.
Adapted from John Wiegley's init.el."
  (require 'auth-source)
  (require 'auth-source-pass)
  (let ((auth (auth-source-search :host host :user user)))
    (if auth
        (let ((secretf (plist-get (car auth) :secret)))
          (if secretf
              (funcall secretf)
            (error "Auth entry for %s@%s has no secret!"
                   user host)))
      (error "No auth entry found for %s@%s" user host))))

(provide 'jutils)
;;; jutils.el ends here
